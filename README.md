# Random User Generator [2021]

Created a Random User Generator Single Page Application

## Project Description
- Angular version 12.2.1
- Get a random User

## Development server

- Run `ng serve` for a dev server
- Navigate to `http://localhost:4200/`
- The app will automatically reload if you change any of the source files

## Randomuser API
- [Page](https://randomuser.me/)
- [JSON-API](https://randomuser.me/api)
- Request URL is set to `https://randomuser.me/api` in `task.service.ts` file.

## Pages Overview

### Main Page
![Random Users Page ](/images/readme/main_page.png "Random Users")

